﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace URLParser
{
    class Program
    {
        List<Match> myList = new List<Match>();
        static void Main(string[] args)
        {
            Console.WriteLine("Напишите html-адрес для парсинга");
            string URLAddress = Console.ReadLine();
            //создаем подключение HTTP и получаем код страницы
            HttpWebRequest proxy_request = (HttpWebRequest)WebRequest.Create(URLAddress);
            proxy_request.Method = "GET";
            proxy_request.ContentType = "application/x-www-form-urlencoded";
            proxy_request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.89 Safari/532.5";
            proxy_request.KeepAlive = true;
            HttpWebResponse resp = proxy_request.GetResponse() as HttpWebResponse;
            string html = "";
            using (StreamReader sr = new StreamReader(resp.GetResponseStream(), Encoding.GetEncoding(1251)))
                html = sr.ReadToEnd();
            html = html.Trim();

            //ищем совпадения по регулярному выражению
            Regex reg_for_proxy = new Regex(@"((https?\:\/\/)|(www.))[a-z0-9.]+[\.][a-z]{2,3}([\/][a-zA-Z0-9_\-\?\=\&\#\/\.]+)*", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            MatchCollection collect_math = reg_for_proxy.Matches(html);

            //выводим список URL в консоль. Через hash реализовано удаление дубликатов
            Hashtable hash = new Hashtable();
            foreach (Match a in collect_math)
            {
                string foundMatch = a.ToString();
                if (hash.Contains(foundMatch) == false)
                    hash.Add(foundMatch, string.Empty);
            }
            foreach (DictionaryEntry item in hash)
            {
                Console.WriteLine(item.Key);
            }
            Console.ReadLine();
        }
    }
}
